/*
	When you play a JUMBLE, you are given a scrambled word that you must unscramble. 
	For instance you may be given "enth" and you would be expected to unscramble it
	to "then".

	Write an application that prompts for a word and displays all possible jumbled 
	combinations of this word. The list should contain only unique combinations
	and should be displayed in descending order.

	For example:  

	if the input was abb, the expected output would be:
	Anagrams of the word "abb" are: bba bab abb 

	if the input was abc, the expected output would be:
	Anagrams of the word "abc" are: cba cab bca bac acb abc
*/
#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

int main (int argc, char * const argv[]) {
	string input;
	vector<string> words;
	vector<string>::iterator it;
	
	//input = "abb";
	input = "abc";
	string anagram(input);
	
	do
	{
		next_permutation(anagram.begin(), anagram.end());
		words.push_back(anagram);
	} while (anagram != input);
	
	sort(words.begin(), words.end());
	reverse(words.begin(), words.end());
	cout << "Anagrams of the word " << input << " are:";
	
	it = words.begin();
	for (; it != words.end(); it++)
		cout << " " << *it;
	
    return 0;
}
