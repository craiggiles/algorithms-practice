/*
	Write a program to determine if a number is a perfect number.
	A perfect number is defined as any positive integer that is 
	equal to the sum of its proper positive divisors.
*/
#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

bool isPerfectNumber(int value) {
	vector<int> numbers;
	vector<int>::iterator it;
	int sum = 0;
	
	cout << "checking number " << value << endl;
	
	//	check for all numbers that go into value evenly
	for (int i = 1; i < value; ++i) {
		if (value % i == 0)
			numbers.push_back(i);
	}
	
	//	loop through the numbers, outputting a "+ value" for every
	//	valid number except "1" which will omit the +
	it = numbers.begin();
	for (; it != numbers.end(); ++it) {
		if (*it != 1) cout << " + ";
		cout << *it;
		sum += *it;
	}
	
	//	output the sum
	cout << " = " << sum << endl;
	
	if (sum == value) return true;	
	return false;
}

int main () {
	int low, high;
	vector<int> results;
	
	//cin >> low >> high;
	low = 6;
	high = 10;
	
	if (low > high) 
		swap(low, high);

	//	for each number between the two selected,
	//	check if that number is a perfect number. If it is,
	//	add it to the 'results' vector.
	for (int i = low; i <= high; ++i) {
		if (isPerfectNumber(i))
			results.push_back(i);
	}
	
	if (results.size() == 0)
		cout << "Set contains no perfect numbers" << endl;
	else {
		cout << "Set contains the following perfect numbers:";
		vector<int>::iterator it = results.begin();
		for (; it != results.end(); it++)
			cout << " " << *it;
	}

    return 0;
}
