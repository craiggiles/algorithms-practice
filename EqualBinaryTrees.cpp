/*
	Write a function that takes 2 binary trees as input, and outputs true if
	these trees are equal to eachother, false otherwise.

	Example:
	1 1 -> True
	2 3 2 3

	2 2 -> False
	3 4 3 5
	Strategy: General recursion

	Function definition:

	public boolean areThetEqual(Tree t1, Tree t2){
	...
}

*/

#include <iostream>

using namespace std;

struct NODE {
	public: 
		NODE(int value) { data = value; }
		int data;
		NODE* left;
		NODE* right;
};

bool areTheyEqual(NODE *t1, NODE *t2);

int main() {
	NODE *t1 = new NODE(1);
	NODE *t1l = new NODE(2);
	NODE *t1r = new NODE(3);
	NODE *t2 = new NODE(1);
	NODE *t2l = new NODE(2);
	NODE *t2r = new NODE(3);

	t1->left = t1l;
	t1->right = t1r;

	t2->left = t2l;
	t2->right = t2r;	

	//should be true
	if (areTheyEqual(t1, t2)) cout << "true" << endl;
	else cout << "false" << endl;

	t1l->left = new NODE(6);
	//should be false
	if (areTheyEqual(t1, t2)) cout << "true" << endl;
	else cout << "false" << endl;

	t1 = NULL;
	//should be false
	if (areTheyEqual(t1, t2)) cout << "true" << endl;
	else cout << "false" << endl;

	return 0;
}

bool areTheyEqual(NODE *t1, NODE *t2) {
	if (t1 == NULL && t2 == NULL) return true;
	if (t1 == NULL && t2 != NULL) return false;
	if (t1 != NULL && t2 == NULL) return false;

	//if data is the same -> 
		//if left tree returns false -> return false
		//else return areTheyEqual(right tree)

	if (t1->data == t2->data) {
		if (!areTheyEqual(t1->left, t2->left)) return false;
		else return areTheyEqual(t1->right, t2->right);
	}

	return false;
}