/**
 * Problem Statement:
 * Find the longest palendrome in a given string
 */
public class LongestPalendrome {
    String t01 = "abcba"; //abcba
    String t02 = "abcde"; //a
    String t03 = "ababcbabcdedcba"; //abcba
    String t04 = "aaaaaaaaaaaaaaab"; //aaaaaaaaaaa

    public static void main(String[] args) {
        LongestPalendrome lp = new LongestPalendrome();
        lp.run();
    }

    public void run() {
        System.out.println("Test Strings:");
        System.out.println(t01);
        System.out.println(t02);
        System.out.println(t03);
        System.out.println(t04);

        String a = getLongestPalindrome(t01);
        System.out.println("Test: " + t01 + " result: " + a);

        a = getLongestPalindrome(t02);
        System.out.println("Test: " + t02 + " result: " + a);

        a = getLongestPalindrome(t03);
        System.out.println("Test: " + t03 + " result: " + a);

        a = getLongestPalindrome(t04);
        System.out.println("Test: " + t04 + " result: " + a);
    }

    public String getLongestPalindrome(String value) {
        String result = new String();

        for (int i = 0; i < value.length(); ++i) {
            for (int j = i; j < value.length(); ++j) {
                if (isPalindrome(value, i, j)) {
                    if (result.length() < value.substring(i,j+1).length())
                        result = value.substring(i,j+1);
                }
            }
        }

        return result;
    }

    boolean isPalindrome(String value, int begin, int end) {
        for (int i = begin, k = end; i <= k; ++i, --k) {
               if (value.charAt(i) != value.charAt(k)) return false;
        }

        return true;
    }
}
