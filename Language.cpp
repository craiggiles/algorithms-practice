/*
	Consider a language of the following strings: the letter A, the letter B, 
	the letter C followed by a string that is in the language, the letter D followed
	by a string that is in the language. For example, these strings are in the language:
	A,CA,CCA,DCA,B,CB,CCB,DB,DCCB; where CAB is not.

	A valid string in this language can be formally defined as follow:
	<s> = A|B|C<s>|D<s>

	Write an application that accepts a string and determines if the given string
	is part of the language or not.

	The following is the sample input:

	A
	a
	ACC
	CCA

	that should produce the following output:

	A is in the language
	a is not in the language
	ACC is not in the language
	CCA is in the language
*/
#include <iostream>
#include <string>
#include <vector>
using namespace std;

bool isValid(string value) {
	string::iterator it;
	it = value.begin();
	switch (*it) {
		case 'A':
		case 'B':
			return (++it == value.end());
		case 'C':
		case 'D':
			return ( isValid(value.substr(1)) );
		default:
			return false;
	}//end switch
}

int main (int argc, char * const argv[]) {
	vector<string> input;
	input.push_back("C");
	input.push_back("a");
	input.push_back("ACC");
	input.push_back("CCA");
	input.push_back("DCA");
	input.push_back("B");
	input.push_back("CB");
	input.push_back("CCB");
	input.push_back("DCCB");
	input.push_back("CAB");
	
	vector<string>::iterator it = input.begin();
	for (; it != input.end(); ++it) {
		if (isValid(*it)) cout << *it << " is in the language" << endl;
		else cout << *it << " is not in the language" << endl;
	}
	return 0;
}
