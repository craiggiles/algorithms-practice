
/**
 * Write a method that takes a string which contains a set of words (a sentence)
 * and rotates those words X number of places. When a word in the first
 * position of the sentence is rotated it should show up at the end of the resulting
 * string.
 */
public class WordRotate {
    public static void main(String[] args) {
        String test = "The quick brown fox jumped over the thing over there";
        String result = rotate(test, 0);

        System.out.println();
        System.out.println("Test: 0");
        result = rotate(test, 0);
        System.out.println(test);
        System.out.println(result);

        System.out.println();
        System.out.println("Test: 3");
        result = rotate(test, 3);
        System.out.println(test);
        System.out.println(result);

        System.out.println();
        System.out.println("Test: 5");
        result = rotate(test, 5);
        System.out.println(test);
        System.out.println(result);

        System.out.println();
        System.out.println("Test: 10");
        result = rotate(test, 10);
        System.out.println(test);
        System.out.println(result);

    }

    private static String rotate(String value, int numb) {
        String result = "";
        String[] split = value.split(" ");

        numb = numb % split.length;

        for (int i = numb; i < split.length; ++i) {
            result += split[i] + " ";
        }

        for (int i = 0; i < numb; ++i) {
            result += split[i] + " ";
        }

        return result;
    }
}
