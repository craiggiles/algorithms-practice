/**
	Craig Giles
	Write a function that takes a linked list of:
	A->B->C->D 

	and rearranges it to :
	B->A->D->C
**/
#include <iostream>

using namespace std;

struct NODE {
	char paydirt;
	NODE* next;
};

NODE* rearrange(NODE* head);
void printNodes(NODE* head);

int main() {

	NODE *a = new NODE();
	NODE *b = new NODE();
	NODE *c = new NODE();
	NODE *d = new NODE();
	NODE *e = new NODE();
	NODE *f = new NODE();
	NODE *g = new NODE();
	NODE *h = new NODE();

	NODE *head = a;

	a->paydirt = 'a';
	b->paydirt = 'b';
	c->paydirt = 'c';
	d->paydirt = 'd';
	e->paydirt = 'e';
	f->paydirt = 'f';
	g->paydirt = 'g';
	h->paydirt = 'h';

	a->next = b;	
	b->next = c;	
	c->next = d;	
	d->next = e;

	e->next = f;	
	f->next = g;	
	g->next = h;	
	h->next = NULL;	

	printNodes(head);
	head = rearrange(head);
	printNodes(head);
}

//a->b->c->d  to  b->a->d->c
NODE* rearrange(NODE* head) {
	NODE *a = NULL;
	NODE *b = NULL;
	NODE *c = NULL;
	NODE *d = NULL;

	if (head == NULL || head->next == NULL) return head; // no nodes or only a
	if (head->next->next == NULL) { //a and b
		a = head;
		b = head->next;

		b->next = a;
		a->next = NULL;

		return b;
	}
	if (head->next->next->next == NULL) { //a->b->c
		a = head;
		b = head->next;
		c = head->next->next;

		b->next = a;
		a->next = c;
		c->next = NULL;

		return b;
	}

	//a b c d
	a = head;
	b = head->next;
	c = head->next->next;
	d = head->next->next->next;
	NODE *newHead = d->next;

	b->next = a;
	a->next = d;
	d->next = c;
	c->next = NULL;
	c->next = rearrange(newHead);
	
	return b;
}

void printNodes(NODE* head) {
	while (head != NULL) {
		cout << head->paydirt << " ";
		head = head->next;
	}
	cout << endl;
}