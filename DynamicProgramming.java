

Code: Knapsack 0/1 problem
------------------------------------------------------------------------------------
#include <iostream>

#define N 100

using namespace std;

int main (int argc, char * const argv[]) {
    int weights[N+1];	//weight for all 100 items
    int values[N+1];	//value for all 100 items
    int W = 10*N;		//weight limit
    
    for (int i=1; i<=N; i++) {
        weights[i] = (i<<10)%97;
        values[i] = (i<<12)%101;
		//printf("%d) w:%d, v:%d\n",i,w[i],v[i]);
    }
	
    int x = N;
	int y = W;
	int D[x+1][y+1];
	
	//initialize table (including base cases to 0)
	for (int i = 0; i <= x; ++i) {
		for (int j = 0; j <= y; ++j) {
			D[i][j] = 0;
		}
	}	

	//build the table
	for(int i = 1; i <= x; i++) {
		for(int j = 1; j <= y; j++) {
			//if the weight of the object is greater than max weight, 
			//don't take the object
			if (weights[i] > y) 
				D[i][j] = D[i-1][j];
			
			//if the weight of the object is greater than the room allowed in the
			//knapsack, we can not take the object
			else if (j - weights[i] < 0) {
				D[i][j] = D[i-1][j];
			}
			
			//Otherwise, make a choice on the most valuable based on
			//information already known
			else {
				D[i][j] = max( values[i] + D[i-1][j-weights[i]] , D[i-1][j] );
			}
		}
	}
		
	//DEBUG: Print table
	cout << "----------------------------- DEBUG: PRINT TABLE ----------------------------------" << endl;
	for (int i = 0; i <= x; ++i) {
		cout << "ROW " << i << ")     " ;
		for (int j = 0; j <= y; ++j) {
			cout << D[i][j] << " | ";
		}
		
		cout << endl; 
	}
	
	printf("Max = %d\n", D[x][y]);
	
    return EXIT_SUCCESS;
}

------------------------------------------------------------------------------------
	Problem #2 : Greedy Selection of Rest Stops
------------------------------------------------------------------------------------
#include <iostream>
#include <vector>

using namespace std;
vector<int> stops;

void getIntermediaryStops(int s[], int n, int m, int start) {
	//test for end of stop
	if (s[start] + m > s[n-1]) {
		cout << "End of the line:" << s[start] << " + " << m  << " > " << s[n-1] << endl;
		stops.push_back(m);
	}
	
	//we haven't reached the end. Go m miles, then backtrack to the previous stop
	else {
		for (int i = start+1; i <= n; ++i) {
			cout << "Testing stop: " << i << endl;
			
			if (s[i] - s[start] > m) {
				if (i == start+1) {
					cout << "Unreachable Destination" << endl;
					break;
				}
				
				cout << "stop " << i << " is too far.. back up one" << endl;
				//we've gone past the allowed miles, go back one
				stops.push_back(i-1);
				getIntermediaryStops(s, n, m, i-1);
				break;
			}
		}
	}
}

int main (int argc, char * const argv[]) {
	//greedy solution index = 0 2 3 7 9 10
    int s[] = { 0, 1, 6, 15, 17, 20, 21, 23, 28, 32, 37 };
	int n = sizeof(s) / sizeof(int);
	
	getIntermediaryStops(s, n, 10, 0);
	
	cout << "{ ";
	vector<int>::iterator it = stops.begin();
	for (; it != stops.end(); it++) {
		cout << *it << " ";
	}
	cout << "}"<<endl;
    return 0;
}
