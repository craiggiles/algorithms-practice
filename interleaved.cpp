/**
	Three strings say A,B,C are given to you. Check weather 3rd string is interleaved from string A and B. 
	Ex: A="abcd" B="xyz" C="axybczd". answer is yes.
*/

#include <iostream>

using namespace std;

bool interleaved(string a, string b, string c);

int main() {
	string a = "abcd";
	string b = "xyz";
	string c = "axybczd";

	bool isInterleaved = interleaved(a, b, c);

	cout << a << " is ";
	if (!isInterleaved) cout << " not ";

	cout << " interleaved with " << b << " to form " << c << endl;
	return 0;
}

bool interleaved(string a, string b, string c) {
	if (c.size() == 0 && a.size() == 0 && b.size() == 0) return true;

	if (c.size() > 0) {
		//if c[0] == a[0] && b[0] -> 
			//if (!interleaved(a[1..n], b, c[1..n])) 
				//return interleaved(a, b[1..n], c[1..n]);
		if (c[0] == a[0] && c[0] == b[0]) {
			if (interleaved(a.substr(1), b, c.substr(1))) return true;
			else return interleaved(a, b.substr(1), c.substr(1));
		}

		//if c[0] == a[0] -> interleaved(a[1..n], b, c[1..n]);
		if (c[0] == a[0]) return interleaved(a.substr(1), b, c.substr(1));		

		//if c[0] == b[0] -> return interleaved(a, b[1..n], c[1..n]);
		if (c[0] == b[0]) return interleaved(a, b.substr(1), c.substr(1));		
	}

	return false;
}