/**
    Write a program that determines the longest sequence of numbers in an array
*/
import java.util.ArrayList;
import java.util.Arrays;

public class LongestSequenceOfNumbers {
    static int[] values = {1, 6, 10, 4, 7, 9, 5, 11, 12, 13, 14, 15, 16, 17, 20};
    //static int[] values = {1, 6, 10, 4, 7, 9, 5 };

    public static void main(String[] args) {
        //sort values
        Arrays.sort(values);

        System.out.println("The longest sequence is: ");

        //determine longest sequence
        printLongestSequence(values);
    }

    private static void printLongestSequence(int [] values) {
        int test_start = 0;
        int start = 0;
        int end = 0;
        int i = 0;

        if (values.length == 0)
            System.out.println("No sequence found");
        else if (values.length == 1){
            start = 0;
            end = 0;
        }
        else {
            for (i = 0; i < values.length-1; ++i) {
                if (values[i+1] - values[i] != 1) {
                    //we've found that i is the last element of the current sequence
                    if ((end-start) <= (i - test_start)){
                        //if the old list is shorter, replace with new list
                        end = i;
                        start = test_start;
                        test_start = i+1;
                    }
                }
            }
        }//end else

        for (i = start; i <= end; ++i)
            System.out.println(values[i]);

    }//end printLongestSequence
}
