/*
	Given an integer array, sort the integer array such that the concatenated integer 
	of the result array is max. e.g. [4, 94, 9, 14, 1] will be sorted to [9,94,4,14,1] 
	where the result integer is 9944141
*/

#include <iostream>
#include <string>
#include <sstream>

using namespace std;

int numbers[] = {4, 94, 9, 14, 1};
int answer = 0;

void testAnswer(int values[], int n) {
	stringstream ss;
	for (int i = 0; i < n; ++i) {
		ss << values[i];
	}

	int value;
	ss >> value;
	if (value > answer) answer = value;
}

void permutations(int values[], int i, int j) {
	if (i == j)
		testAnswer(values, j);
	else {
		for (int x = 0; x < j; ++x) {
			swap(values[x], values[i]);
			permutations(values, i+1, j);
			swap(values[x], values[i]);
		}
	}
}

int main() {
	int size = sizeof(numbers) / sizeof(int);

	cout << "Starting: ";
	for (int i = 0; i < size; ++i){
		cout << numbers[i];
	}
	cout << endl;

	for (int i = 0; i < size; ++i){
		permutations(numbers, i, size);
	}

	cout << endl;
	cout << "Ending answer: " << answer << endl;

	return 0;
}

