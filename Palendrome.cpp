/**
	Craig Giles
	Palendrome: Detects if a given string is a palendrome
*/
#include <iostream>
#include <stack>
#include <vector>

using namespace std;

bool isPalendrome(const string value);

int main(int argc, const char *argv[]) {
	string test01 = "aaaaa";
	string test02 = "a";
	string test03 = "abc";
	string test04 = "race car";
	string test05 = "racecar";

	cout << test01 << " ";
	if (isPalendrome(test01)) cout << "is ";
	else cout << "is not ";
	cout << "a palendrome" << endl;

	cout << test02 << " ";
	if (isPalendrome(test02)) cout << "is ";
	else cout << "is not ";
	cout << "a palendrome" << endl;

	cout << test03 << " ";
	if (isPalendrome(test03)) cout << "is ";
	else cout << "is not ";
	cout << "a palendrome" << endl;

	cout << test04 << " ";
	if (isPalendrome(test03)) cout << "is ";
	else cout << "is not ";
	cout << "a palendrome" << endl;

	cout << test05 << " ";
	if (isPalendrome(test05)) cout << "is ";
	else cout << "is not ";
	cout << "a palendrome" << endl;


	return 0;
}

bool isPalendrome(const string value) {
	string tmp(value);
	stack<char> chars;

	string::iterator itr = tmp.begin();
	for (; itr != tmp.end(); ++itr) {
		chars.push(*itr);
	}

	for (itr = tmp.begin(); itr != tmp.end(); ++itr) {
		if (*itr != chars.top()) return false;
		else chars.pop();
	}

	

	return true; 
}