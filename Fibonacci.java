
import java.util.Vector;

/**
 *
 * @author gilesc
 */
public class DynamicProgramming {
    public static void main(String[] args) {
        //fib sequence
        getFibSequence(10);
    }
    
    static void getFibSequence(int maxIndex) {
        int[] fib = new int[maxIndex];
        
        makeTable(fib);
        
        for (int i = 0; i < fib.length; ++i)
            System.out.println(fib[i] + ", ");
    }

    private static void makeTable(int[] fib) {
        fib[0] = 0;
        fib[1] = 1;
        for (int i = 2; i < fib.length; ++i) {
            fib[i] = fib[i-1] + fib[i-2];
        }
    }
}
