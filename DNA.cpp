/*
 Bioinformatics is the application of computer technology to the management of
 biological information. Biologists do a great deal of research on DNA sequences.
 Many of the problems that interest biologists turn out to be related to string
 algorithms.
 For example given two DNA sequences, which parts of one sequence correspond to
 which parts of the other? No two DNA sequences from different individuals are
 the same; this sequence alignment helps to determine the common base sequences
 for all humans. Presumably the base sequences that we all have in common are
 what makes us "human".
 
 A "subsequence" of a string, s1, is a subset of characters from s1 that appear
 in the same order they do in s2. For example, C G G T is a subsequence of
 A C A G A G T.
 A "motif" is a short subsequence that occurs frequently in different sequences.
 Such a phenomenon might mean that the genes containing that motif perform similar
 functions or evolved one from the other. A motif common to fish and mammals
 might indicate evolutionary paths. A motif common to diabetes patients who reject
 a particular drug treatment but not common to patients who respond to the treatment
 might uncover the responsible gene.
 
 What's all this got to do with strings? Everything! DNA can be represented as
 strings using an alphabet of 4 letters: {A, C, T, G}
 
 The bread and butter of bioinformatics is the search for strands of DNA inside
 other strands.
 
 Write a program that accepts two strings of characters and determines whether
 or not the first string is contained in the second string.
 
 INPUT:
 C G G T
 A C A G A G T
 
 OUTPUT:
 true
 */
#include <iostream>

using namespace std;
int main (int argc, char * const argv[]) {
	string first;
	string second;
	string::iterator it;
	bool complete = true;
	
	while (getline(cin, first) && getline(cin, second))
	{
		string tmp;
		it = first.begin();
		for (; it != first.end(); it++)
			if ( isalpha(*it)) tmp += *it;
		
		first = tmp;
		it = first.begin();
		for (; it != first.end(); it++)
		{
			size_t pos = second.find_first_of(*it);
			
			if (pos > second.length()) { complete = false; break; }
			string sub = second.substr(pos + 1);
			second = sub;
		}
		
		if (complete) cout << "true" << endl;
		else  cout << "false" << endl;
	}
	
    return 0;
}
